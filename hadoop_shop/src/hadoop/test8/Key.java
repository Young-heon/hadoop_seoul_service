package hadoop.test8;

import hadoop.test7.codeKey;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class Key implements WritableComparable <Key> {
	private String location;
	private Integer productNum;

	
	public Key(){}
	
	public Key(String location, Integer productNum){
		this.location = location;
		this.productNum = productNum;
	}
	
	public String getLocation(){
		return location;
	}
	public Integer getProductNum(){
		return productNum;
	}
	
	public void setLocation(String location){
		this.location = location;
	}
	public void setProductNum(Integer productNum){
		this.productNum = productNum;
	}
	
	
	@Override
	public String toString(){
		return (new StringBuilder()).append(",").append(location).append(",").append(productNum).append(",").toString();
	}
	
	@Override
	public void readFields(DataInput in) throws IOException{
		location = WritableUtils.readString(in);
		//location = in.readUTF();
		productNum = in.readInt();
	}
	
	@Override
	public void write(DataOutput out) throws IOException{
		WritableUtils.writeString(out,location);
		//out.writeUTF(location);
		out.writeInt(productNum);
	}
	
	@Override
	public int compareTo(Key key){
		int result = location.compareTo(key.location);
		if(0 == result)
		{
			result = productNum.compareTo(key.productNum);
		}
		return result;
	}

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		String s1 = ((Key)arg0).location;
		Integer s2 = ((Key)arg0).productNum;
		if(location.equals(s1) && productNum.equals(s2)) {
			return true;
		}
		else {
			return false;
		}
	}
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return (location+productNum).hashCode();
	}
	
}
