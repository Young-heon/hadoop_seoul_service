package hadoop.test8;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class MinFeeMrTest {
	
	MinFeeMapper mapper = new MinFeeMapper();
	MinFeeReducer reducer = new MinFeeReducer();

	MapDriver<LongWritable, Text, Key, IntWritable> mapDriver;
	ReduceDriver<Key, IntWritable, Key, IntWritable> reduceDriver;
	MapReduceDriver<LongWritable, Text, Key, IntWritable, Key, IntWritable> mapReduceDriver;

	@Before
	public void setUP() {
		
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

	@Test
	public void MapperTest() {
///////////////////////////////////////////////////////////////////////////////////
// Case of Success
		
		mapDriver
				.withInput(
						new LongWritable(1),
						new Text(
								"285,해운대,김영자,N,650581107     ,반포동,19-4,59, 535-2412,2007-02-12 00:00:00.0,1110,한식,2110,설렁탕,7000,201304"));
		mapDriver.withOutput(new Key("650581107", 2110), new IntWritable(7000));
		
///////////////////////////////////////////////////////////////////////////////////
		
///////////////////////////////////////////////////////////////////////////////////
//Case of Fail
		
//		mapDriver
//		.withInput(
//				new LongWritable(1),
//				new Text(
//						"285,해운대,김영자,N,650581107     ,반포동,19-4,59, 535-2412,2007-02-12 00:00:00.0,1110,한식,2110,설렁탕,7000,201304"));
//mapDriver.withOutput(new Key("650581107", 2110), new IntWritable(8000)); //7000 -> 8000 Error!
		
///////////////////////////////////////////////////////////////////////////////////
		try {
			mapDriver.runTest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
	
	@Test
	public void ReducerTest() {
		List<IntWritable> values = new ArrayList<IntWritable>();
		List<IntWritable> values2 = new ArrayList<IntWritable>();

		
///////////////////////////////////////////////////////////////////////////////////
//Case of Success
		
		values.add(new IntWritable(7000));
		values2.add(new IntWritable(2500));
		
		reduceDriver.withInput(new Key("640581107", 2110), values);
		reduceDriver.withInput(new Key("640581120", 2110), values2);
		reduceDriver.withOutput(new Key("640581107", 2110), new IntWritable(
				7000));
		reduceDriver.withOutput(new Key("640581120", 2110), new IntWritable(
				2500));
		
///////////////////////////////////////////////////////////////////////////////////
	
///////////////////////////////////////////////////////////////////////////////////
//Case of Fail		
		
//		values.add(new IntWritable(4500));
//		values2.add(new IntWritable(20000));
//		
//		//3799,동원각,최창섭,N,620550101,봉천동,916-16, 874-1777,1120,중식,2260,자장면,4500,201303
//		//4827,김창숙미용실,현옥순,N,620610101,봉천동,888-22, 872-7229,1310,미용업,2630,미용료 (파마),20000,201303
//		reduceDriver.withInput(new Key("620550101", 2260), values);
//		reduceDriver.withInput(new Key("620610101", 2630), values2);
//		reduceDriver.withOutput(new Key("620550101", 2260), new IntWritable(
//				5500));// 4500 -> 5500 Error!
//		reduceDriver.withOutput(new Key("620610101", 2630), new IntWritable(
//				30000));// 20000 -> 30000 Error!
		
///////////////////////////////////////////////////////////////////////////////////
		try {
			reduceDriver.runTest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

	@Test
	public void MapReducerTest() {
	
///////////////////////////////////////////////////////////////////////////////////
//Case of Success
		
		mapReduceDriver.withInput(
						new LongWritable(1),
						new Text("285,해운대,김영자,N,650581107     ,반포동,19-4,59, 535-2412,2007-02-12 00:00:00.0,1110,한식,2110,설렁탕,7000,201304"));
	
		mapReduceDriver.withOutput(new Key("650581107", 2110), new IntWritable(7000));
		
///////////////////////////////////////////////////////////////////////////////////
		
///////////////////////////////////////////////////////////////////////////////////
//Case of Fail	
		
		//267147,이모네보신탕,-,N,560535105,영등포동4가, 98-2,02  26336656,1110,한식,2150,삼계탕,10000,201304
//		mapReduceDriver.withInput(
//				new LongWritable(1),
//				new Text("267147,이모네보신탕,-,N,560535105,영등포동4가, 19-4,59, 535-2412,2007-02-12 00:00:00.0,1110,한식,2110,설렁탕,10000,201304"));
//
//		mapReduceDriver.withOutput(new Key("560535105", 2150), new IntWritable(10000));
		
///////////////////////////////////////////////////////////////////////////////////
		try {
			mapReduceDriver.runTest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}