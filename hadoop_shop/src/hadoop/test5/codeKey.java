package hadoop.test5;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;


public class codeKey implements WritableComparable <codeKey> {

	
	private String locate;
	private String product;
	
	public codeKey(){}
	
	public codeKey(String locate, String product){
		this.locate = locate;
		this.product = product;
	}
	public String getLocation()
	{
		return locate;
	}
	public void setLocation(String locate){
		this.locate=locate;
	}
	public String getProduct(){
		return product;
	}
	public void setProduct(String product){
		this.product = product;
	}
	
	@Override
	public String toString(){
		return (new StringBuilder()).append(locate).append(",").append(product).toString();
	}
	@Override
	public void readFields(DataInput in) throws IOException{
		locate = WritableUtils.readString(in);
		product = in.readUTF();
	}
	@Override
	public void write(DataOutput out) throws IOException{
		WritableUtils.writeString(out,locate);
		out.writeUTF(product);
	}

	@Override
	public int compareTo(codeKey key) {
		//int result = locate.compareTo(key.locate);
		//if(0==result){
		int	result = product.compareTo(key.product);
	//	}
		
		return result;
	}
	
}

