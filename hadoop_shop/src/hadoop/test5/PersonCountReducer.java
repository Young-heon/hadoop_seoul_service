package hadoop.test5;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class PersonCountReducer extends Reducer<codeKey, IntWritable, codeKey, IntWritable> {
	private IntWritable result = new IntWritable();
	
	public void reduce(codeKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
	/*int sum = 0;
	for(IntWritable value :values)
		sum+=value.get();  //값을 더한다
	result.set(sum);
	context.write(key, result);
	*/
	int minValue = Integer.MAX_VALUE;  //최대 값을 넣어둔다. //방지
	for(IntWritable value : values)
	{
		minValue = Math.min(minValue, value.get()); //MATH 함수 이용하여 최소값을 찾는다.
	}
	context.write(key, new IntWritable(minValue)); // 그 값을 쓴다.
	}

}
