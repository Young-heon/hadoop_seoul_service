package hadoop.test7;
/*
복합키 비교기는 복합키의 정렬 순서를 부여하기 위한 클래스입니다.
복합키는 비교기를 두개의 복하비를 비교하게 되며, 각 멤버변수를 비교해 정렬 순서를 정합니다.
항공 운항 지연 데이터에 대한 복합키 비교기는 아래와 같다.
*/
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class codeKeyComparator extends WritableComparator {

/*	org.apache.hadoop.io.WritableComparalbe 인터페이스를 구현된 객체를 비교하려면 반드시
	org.apache.hadoop.io.WritableComparator를 상속받아서 비교기를 구현
*/	
	
	protected codeKeyComparator(){
		super(codeKey.class,true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	//두개의 복합키를 비교하려면 compare 메서드를 오버라이드해서 구현
	public int compare(WritableComparable w1, WritableComparable w2){
		
/*		compare 메서드는 이미 org.apache.hadoop.io.WriatableComparator에 구현되어 있지만
		객체를 스트림에서 조회한 값을 비교하게 되므로 정환한 정렬 순서를 부과할 수 없다.
		그래서 메서드를 재정의해서 맴버 변수를 비교하는 로직을 구현해야 한다.
		compare 메서드에서는 우선 파라미터를 전달받은 두 개의 WritableComparable 객체를 
		codeKey로 클래스로 변환합니다. 그래야만 codeKey에 선언한 멤버 변수를 조회 가능
*/		
		codeKey k1 = (codeKey) w1;
		codeKey k2 = (codeKey) w2;
/*
		codeKey로 변환되었으면 우선 지역코드를 비교합니다.
		이때 값의 비교는 String 클래스에서 제공하는 compareTo 메서드를 이용
		compareTo 메서드에서는 k1과 k2의 지역코드값이 동일한 경우에는 0을, 
		k1의 지역코드값이 k2의 연도값보다 클 경우에는 1을
		k1의 지역코드값이 k2보다 작을 경우 -1을 반환
		compareTo를 호출한 값이 0이 아닐 경우, 해당 값이 반환하도록 구현
		지역코드가 일치하지 않을 경우 이부분에서 정렬 순서 정해짐
		*/
		
		int cmp = k1.getLocation().compareTo(k2.getLocation());
		if(cmp!=0){
			return cmp;
		}
		
		return k1.getProduct() == k2.getProduct() ? 0 : (k1.getProduct() < k2.getProduct() ? -1 : 1);
		/*
		만약 지역코드가 일치할 경우에는 그 다음 줄에 있는 코드가 실행된다.
		이 코드는 상품코드를 비교해서 k1과 k2의 상품코드를 비교에서 같을 경우 0
		k1의 상품코드가 k2의 상품코드 값보다 작을 경우에는 -1을
		k1의 상품코드가 k2의 상품코드 값보다 클 경우에는 1을 반환하여 
		상품코드를 기준으로 오름차순으로 정렬
		*/
		
		
	}
}
