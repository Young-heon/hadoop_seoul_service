package hadoop.test7;
/*
파티셔너는 맵 태스크의 출력 데이터를 리듀스 태스크의 입력 데이터를 보낼지 결정하고
이렇게 파티셔닝된 데이터는 맵 태스크의 출력 데이터의 키의 값에 따라 정렬 수행
구현되는 그룹키 파티셔너는 그룹핑 키로 사용하는 지역코드에 대한 파티셔닝을 수행
아래는 그룹키 파티셔너의 코드 구현 내용임
*/
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;
/*
맵리듀스는 잡에서 사용하는 파티셔너는 반드시 
org.apache.hadoop.mapreduce.partitioner를 상속받아서 구현
이때 파티셔너 설정하는 두개의 파라미터는 Mapper의 출력 데이터의 키와 값에 해당하는 파라미터
*/

/*파티셔너는 getPartition 메서드를 호출해서 파티셔닝 번호를 조회
getPartiton 메서드를 재정의해서 구현한 후 
지역코드에 따른 해시 코드를 조회하여 파티션 번호를 생성
*/
public class groupKeyPartitioner extends Partitioner<codeKey, IntWritable>{

	@Override
	public int getPartition(codeKey key, IntWritable val, int numPartitions){
		int hash = key.getLocation().hashCode();
		int partition = hash%numPartitions;
		return partition;
	}
}
