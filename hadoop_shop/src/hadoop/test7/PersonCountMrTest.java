package hadoop.test7;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.hadoop.mrunit.mapreduce.MapReduceDriver;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.junit.Before;
import org.junit.Test;

public class PersonCountMrTest {
	
	MapDriver<LongWritable, Text, codeKey, IntWritable> mapDriver;
	ReduceDriver<codeKey, IntWritable, codeKey, IntWritable> reduceDriver;
	MapReduceDriver<LongWritable, Text, codeKey, IntWritable, codeKey, IntWritable> mapReduceDriver;
	
	@Before
	public void setUP()
	{
		PersonCountMapper mapper = new PersonCountMapper();
		PersonCountReducer reducer = new PersonCountReducer();
		mapDriver = MapDriver.newMapDriver(mapper);
		reduceDriver = ReduceDriver.newReduceDriver(reducer);
		mapReduceDriver = MapReduceDriver.newMapReduceDriver(mapper, reducer);
	}
	
	@Test
	public void testMapper()
	{
		/*
		 * 
		 * 예제 데이터
		281,대전식당,신철순,N,650581107,반포동,04월 19일, 536-1090,1110,한식,2110,설렁탕,6500,201303
		281,대전식당,신철순,N,650581107,반포동,04월 19일, 536-1090,1110,한식,2120,냉면(물),6000,201303
		281,대전식당,신철순,N,650581107,반포동,04월 19일, 536-1090,1110,한식,2130,비빔밥,6000,201303
		281,대전식당,신철순,N,650581107,반포동,04월 19일, 536-1090,1110,한식,2160,김치찌개 백반,6000,201303
		285,해운대,김영자,N,650581107,반포동,04월 19일, 535-2412,1110,한식,2110,설렁탕,7000,201304
		285,해운대,김영자,N,650581107,반포동,04월 19일, 535-2412,1110,한식,2120,냉면(물),6000,201304
		285,해운대,김영자,N,650581107,반포동,04월 19일, 535-2412,1110,한식,2130,비빔밥,6000,201304
		285,해운대,김영자,N,650581107,반포동,04월 19일, 535-2412,1110,한식,2160,김치찌개 백반,6000,201304
		*/
		//사용한 또다른 예제
		/*public void testCombiningMapper() throws Exception {
  new MapDriver<LongWritable,Text,Text,TemperatureAveragingPair>()
   .withMapper(new AverageTemperatureCombiningMapper())
   .withInput(new LongWritable(4),new Text(temps[3]))
   .withOutput(new Text("190101"),new TemperatureAveragingPair(-61,1))
   .runTest();
			 }
		*/
		
		mapDriver.withInput(new LongWritable(1), new Text
				("285,해운대,김영자,N,650581107,반포동,04월 19일, 535-2412,1110,한식,2110,설렁탕,7000,201304"	));
		mapDriver.withOutput(new codeKey("650581107", 2110), new IntWritable(7000));
		try {
			mapDriver.runTest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testReducer()
	{
		List<IntWritable> values = new ArrayList<IntWritable>();
		List<IntWritable> values2 = new ArrayList<IntWritable>();
		values.add(new IntWritable(7000));
		values2.add(new IntWritable(2500));
		reduceDriver.withInput(new codeKey("640581107",2110), values);
		reduceDriver.withInput(new codeKey("640581120",2110), values2);
		reduceDriver.withOutput(new codeKey("640581107",2110),new IntWritable(7000));
		reduceDriver.withOutput(new codeKey("640581120",2110),new IntWritable(2500));
		try {
			reduceDriver.runTest();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
//	
}
