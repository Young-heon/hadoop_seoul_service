package hadoop.test7;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;


public class PersonCountReducer extends Reducer<codeKey, IntWritable, codeKey, IntWritable> {
	
	private codeKey outputKey = new codeKey();
	private IntWritable result = new IntWritable();
	
	public void reduce(codeKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
	
	//String [] colums = key.getLocation().split(",");
	
	Integer bProduct = key.getProduct();
	
	int minValue = Integer.MAX_VALUE;
	for (IntWritable value : values) 
	{
		if (bProduct != key.getProduct()) {
				minValue = Math.min(minValue, value.get());
				result.set(minValue);
				outputKey.setLocation(key.getLocation());
				outputKey.setProduct(bProduct);
				context.write(outputKey, result);
				minValue = Integer.MAX_VALUE;
			}
			minValue = value.get();
			//minValue = Math.min(minValue, value.get());
			
			bProduct = key.getProduct();
		if (key.getProduct() == bProduct) {
			outputKey.setLocation(key.getLocation());
			outputKey.setProduct(key.getProduct());
			result.set(minValue);
			context.write(outputKey, result);
		}	
	
	}
	}
}
	 	
	
	/*int minValue = Integer.MAX_VALUE;  //최대 값을 넣어둔다. //방지
	for(IntWritable value : values)
	{
		minValue = Math.min(minValue, value.get()); //MATH 함수 이용하여 최소값을 찾는다.
	}
	context.write(key, new IntWritable(minValue)); // 그 값을 쓴다.
	
	}*/


