package hadoop.test7;
/*
리듀서는 그룹키 비교기를 사용해 같은 지역코드에 
해당하는 모든 데이터를 하나의 Reducer 그룹에서 처리 할 수 있다.
*/
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

/*
복합키 비교기에서 구현된 것처럼 
org.apache.hadoop.io.WritableComparator를 상속바아 클래스 선언
*/
public class groupKeyComparator extends WritableComparator{

	protected groupKeyComparator(){
		super(codeKey.class,true);
	}
	/*
	그룹핑 키값인 지역코드를 비교하는 처리가 필요
	compare 메서드에 두개의 복합키의 지역코드를 비교하는 코드를 선언
	*/	
		@SuppressWarnings("rawtypes")
		@Override
		public int compare(WritableComparable w1, WritableComparable w2){
		codeKey k1 = (codeKey) w1;
		codeKey k2 = (codeKey) w2;
		
		return k1.getLocation().compareTo(k2.getLocation());
	}
}
