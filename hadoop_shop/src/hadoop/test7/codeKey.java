package hadoop.test7;
/*
복합키는 기존의 키값을 조합한 일종의 키 집합 클래스
아래 코드는 복합키 구현한 코드
*/

import hadoop.test7.codeKey;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

//복합키를 사용하기 위해 WritableComparable 인터페이스 구현
//Writable Comparable의 피라미터는 바로 자신인 codeKey로 설정
public class codeKey implements WritableComparable <codeKey> {

/*
WriatableComparable 인터페이스 구현체에서는 반드시 readFields, write, compareTo메서드가 구현되어야 함
readFields 메서드는 입력스트림에서 지역코드와 상품코드를 조회하고
write 메서드는 출력 스트림에 지역코드와 상품코드를 출력한다.
이때 스트림에서 데이터를 읽고, 출력하는 것은 하둡에서 제공하는 WritableUtils를 이용합니다.
compareTo메서드는 복합키와 복합키를 비교해서 순서를 정할 때 사용
*/	
	private String locate;
	private Integer product;

/*	지역코도는 String 타입의 멤버변수, 상품코드는 Integer 타입의 변수로 정의
	지역코드와 상품코드를 사용하는 생성자와 setter, getter 메서드도 함께 구현
*/
	public codeKey(){}
	
	public codeKey(String locate, Integer product){
		this.locate = locate;
		this.product = product;
	}
	public String getLocation()
	{
		return locate;
	}
	public void setLocation(String locate){
		this.locate=locate;
	}
	public Integer getProduct(){
		return product;
	}
	public void setProduct(Integer product){
		this.product = product;
	}
	
	@Override
	public String toString(){
		return (new StringBuilder()).append(locate).append(",").append(product).toString();
/*
	매퍼와 리듀서에서는 복합키의 toString 메서드를 호출해서 값을 출력
	toString을 정의하지 않는다면 자바에서 사용하는 클래스 식별자가 출력되어 버린다.
	toString메서드를 재정의해서 출력을 어떤 식으로 할지 구현해줍니다.
	*/
	}
	@Override
	public void readFields(DataInput in) throws IOException{
		locate = WritableUtils.readString(in);
		product = in.readInt();
	}
	@Override
	public void write(DataOutput out) throws IOException{
		WritableUtils.writeString(out,locate);
		out.writeInt(product);
	}

	@Override
	public int compareTo(codeKey key) {
		int result = locate.compareTo(key.locate);
		if(0 == result)
		{
			result = product.compareTo(key.product);
		}
		return result;
	}
	
	//교수가 추가한 소스 시작
	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		String s1 = ((codeKey)arg0).locate;
		Integer s2 = ((codeKey)arg0).product;
		if(locate.equals(s1) && product.equals(s2)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return (locate + product).hashCode();
	}
	
	//끝 숙지 필요.
}

