package hadoop.test3;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PersonCountMapper extends Mapper <LongWritable, Text, Text, IntWritable>{

	private final static IntWritable outputValue = new IntWritable(1);
	private Text outputKey = new Text();
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
		if(key.get()>0)
		{
			String [] colums = value.toString().split(",");
			if(colums !=null&&colums.length>0){
				try{
					outputKey.set(colums[0]+"_"+colums[13]); //업소명_품목(상품명)을 키값으로 설정
					if(!colums[15].equals("NA")){
						int PC = Integer.parseInt(colums[15]);  //상품값을 PC변수에 설정.
						if(PC>0){
							outputValue.set(PC);
							context.write(outputKey,outputValue); //키와 밸류값 생성
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
}
