package hadoop.test;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class DataKeyComparator extends WritableComparator{

	protected DataKeyComparator(){
		super(Datakey.class,true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		DataKey k1 = (DataKey) w1;
		DataKey k2 = (Datakey) w2;
		
		int cmp = k1.getYear().compareTo(k2.getYear());
		if(cmp!=0)
		{
			return cmp;
		}
		
		return k1.getMonth()==k2.getMonth() ? 0:(k1.getMonth()<k2)
				.getMonth() ? -1 : 1);
		}
	
}
