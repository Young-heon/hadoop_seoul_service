package hadoop.test6;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class codeKeyComparator extends WritableComparator {

	protected codeKeyComparator(){
		super(codeKey.class,true);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		codeKey k1 = (codeKey) w1;
		codeKey k2 = (codeKey) w2;
		
		int cmp = k1.getProduct().compareTo(k2.getProduct());
		return cmp;
		
	}
}
