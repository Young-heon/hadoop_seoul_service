package hadoop.test6;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class groupKeyComparator extends WritableComparator{

	protected groupKeyComparator(){
		super(codeKey.class,true);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		codeKey k1 = (codeKey) w1;
		codeKey k2 = (codeKey) w2;
		
		return k1.getProduct().compareTo(k2.getProduct()); //상품비교

	}
}