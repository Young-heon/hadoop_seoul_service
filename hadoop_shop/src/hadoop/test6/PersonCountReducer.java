package hadoop.test6;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;


public class PersonCountReducer extends Reducer<codeKey, IntWritable, codeKey, IntWritable> {
	private IntWritable result = new IntWritable();
	
	public void reduce(codeKey key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
	int sum = 0;
	for(IntWritable value :values)
		sum+=value.get();
	result.set(sum);
	context.write(key, result);
	}

}
