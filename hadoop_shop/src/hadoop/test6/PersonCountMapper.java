package hadoop.test6;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class PersonCountMapper extends Mapper <LongWritable, Text, codeKey, IntWritable>{

	private final static IntWritable outputValue = new IntWritable(1); //map 출력값
	
	private codeKey outputKey = new codeKey(); //map 출력키
	
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
		if(key.get()>0)
		{
			String [] colums = value.toString().split(",");
			if(colums !=null&&colums.length>0){
				try{
					if(!colums[12].equals("NA")){
						int PC = Integer.parseInt(colums[12]);  //상품가격 PC변수에 설정.
						if(PC>0){
							outputKey.setLocation("LocateNum_"+colums[4]); //지역 번호
							outputKey.setProduct(new String ("ProNum_"+colums[10])); // 상품 번호
							outputValue.set(PC);
							context.write(outputKey,outputValue); //키와 밸류값 생성
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}
}
