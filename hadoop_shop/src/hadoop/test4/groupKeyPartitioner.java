package hadoop.test4;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class groupKeyPartitioner extends Partitioner<codeKey, IntWritable>{

	@Override
	public int getPartition(codeKey key, IntWritable val, int numPartitions){
		int hash = key.getLocation().hashCode();
		int partition = hash%numPartitions;
		return partition;
	}
}
