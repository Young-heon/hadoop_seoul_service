package hadoop.test4;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class groupKeyComparator extends WritableComparator{

	protected groupKeyComparator(){
		super(codeKey.class,true);
	}
	@SuppressWarnings("rawTypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		codeKey k1 = (codeKey) w1;
		codeKey k2 = (codeKey) w2;
		
		return k1.getLocation().compareTo(k2.getLocation());
	}
}
