package hadoop.test4;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class codeKeyComparator extends WritableComparator {

	protected codeKeyComparator(){
		super(codeKey.class,true);
	}
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		codeKey k1 = (codeKey) w1;
		codeKey k2 = (codeKey) w2;
		
		int cmp = k1.getLocation().compareTo(k2.getLocation());
		return cmp;
		
		
		//return k1.getProduct()==k2.getProduct()? 0:(k1.getProduct() < k2.getProduct()?-1:1);
	}
}
