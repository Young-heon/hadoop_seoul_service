package hadoop.test2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class sumDriver {
	public static void main(String args) throws Exception{
		Configuration conf = new Configuration();
		
		//입출력 데이터 경로 확인 
		
		if(args.length() !=2){
			System.err.println("Usage : Sum values <input><output>");
			System.exit(2);
		}
		
		//Job 이름 설정
		Job job = new Job(conf, "values Sum");
		
		job.setJarByClass(sumDriver.class);
		
		job.setMapperClass(ValueMapper.class);
		
		job.setReducerClass(valueReducer.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		
		job.waitForCompletion(true);
	}

}
