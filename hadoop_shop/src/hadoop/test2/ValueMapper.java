package hadoop.test2;

import java.io.IOException;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class ValueMapper extends Mapper<LongWritable,Text, Text, IntWritable> {
	//map output value
	private final static IntWritable outputValue  = new IntWritable(1);
	
	//map output key
	private Text outputKey = new Text();
	
	public void map(LongWritable key, Text value, Context context)
 throws IOException, InterruptedException {
		if(key.get()>0){
			//comma seperate.
			String[] colums = value.toString().split(",");
			//String[] ddaohm = value.toString().split(" \" ");  //" remove
			
			if(colums != null && colums.length > 0){
				try{
					//output key preperence
					outputKey.set(colums[0]+","+colums[1]);
					if(!colums[15].equals("NA")){
						int sumValue = Integer.parseInt(colums[15]);
						if(sumValue>0){
							outputValue.set(sumValue);
							context.write(outputKey, outputValue);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
							
			
			
			
		}
	}

}
