package minimumFee;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class GroupKeyComparator extends WritableComparator{
	protected GroupKeyComparator(){
		super(Key.class,true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		Key k1 = (Key) w1;
		Key k2 = (Key) w2;
		
		return k1.getLocation().compareTo(k2.getLocation());
	}
	
}

