package minimumFee;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class GroupKeyPartitioner extends Partitioner<Key, IntWritable> {
	
	@Override
	public int getPartition(Key key, IntWritable val, int numPartitions){
		int hash = key.getLocation().hashCode();
		int partition = hash % numPartitions;
		return partition;
	}

}
