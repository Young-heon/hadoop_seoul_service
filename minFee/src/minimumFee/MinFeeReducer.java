package minimumFee;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;


public class MinFeeReducer extends Reducer<Key, IntWritable, Key, IntWritable>{
	

	private IntWritable result = new IntWritable();
	private Key outputKey = new Key();
	
	public void reduce(Key key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
	
	
		int minValue = Integer.MAX_VALUE;
		for(IntWritable value : values){
			minValue = Math.min(minValue, value.get());
			outputKey.setLocation(key.getLocation());
			outputKey.setProductNum(key.getProductNum());
			outputKey.setShopNum(key.getShopNum());
		}
		result.set(minValue);
		context.write(outputKey, result);
	}
}
