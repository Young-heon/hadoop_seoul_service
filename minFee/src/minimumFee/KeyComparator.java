package minimumFee;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class KeyComparator extends WritableComparator{

	protected KeyComparator(){
		super(Key.class, true);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public int compare(WritableComparable w1, WritableComparable w2){
		Key k1 = (Key) w1;
		Key k2 = (Key) w2;
		
		int result = k1.getLocation().compareTo(k2.getLocation());
		if(0 == result)
			result = k1.getProductNum().compareTo(k2.getProductNum());
		return result;
		
		
		/*		int cmp = k1.getLocation().compareTo(k2.getLocation());
		if(cmp!=0){
			return cmp;
		}
		return k1.getProductNum() == k2.getProductNum() ? 0 : (k1.getProductNum() < k2.getProductNum()? -1 : 1);
		
	
		//return k1.getLocation().compareTo(k2.getLocation());
		//return k1.getProductNum().compareTo(k2.getProductNum());
	*/
	}

}


