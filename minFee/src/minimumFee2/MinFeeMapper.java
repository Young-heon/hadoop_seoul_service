package minimumFee2;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MinFeeMapper extends Mapper<LongWritable, Text, Key, IntWritable> {
	private final static IntWritable outputValue = new IntWritable();
	
	private Key outputKey = new Key();
	public static final Log LOGGER = LogFactory.getLog(MinFeeMapper.class.getName());

	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException{
		if(key.get()>0){
			String [] colums = value.toString().split(",");
			if(colums !=null && colums.length>0){
				try{
					if(!colums[14].equals("NA")){
						int fare = Integer.parseInt(colums[14]);
						if(fare > 100){
							outputKey.setLocation(colums[4].trim());
							outputKey.setProductNum(new Integer(colums[12]));
							//outputKey.setShopNum(new Integer(colums[0]));
							outputValue.set(fare);
							//LOGGER.info("Key: " + outputKey.getLocation() +", " + outputKey.getProductNum()+", "+outputKey.getShopNum());
							context.write(outputKey, outputValue);
						}
					}
				} catch(Exception e){
					e.printStackTrace();
				}
			}
			
		}
	}

}
