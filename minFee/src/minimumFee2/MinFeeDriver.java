package minimumFee2;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class MinFeeDriver extends Configured implements Tool{
	public int run(String [] args) throws Exception{
		String[] otherArgs = new GenericOptionsParser(getConf(),args).getRemainingArgs();
		if(args.length!=2){
			System.err.println("Usage :DepartureDelayCount <input> <output>");
			System.exit(2);
		}
		//JOB 이름 설정
		Job job = new Job(getConf(), "MinFeeDriver");
		
		FileInputFormat.addInputPath(job, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job, new Path(otherArgs[1]));

		job.setJarByClass(MinFeeDriver.class);
		
		//job.setPartitionerClass(GroupKeyPartitioner.class);
		//job.setGroupingComparatorClass(GroupKeyComparator.class);
		//job.setSortComparatorClass(KeyComparator.class);
		
		job.setMapperClass(MinFeeMapper.class);
		job.setReducerClass(MinFeeReducer.class);
		
		job.setMapOutputKeyClass(Key.class);
		job.setMapOutputValueClass(IntWritable.class);
		
		job.setInputFormatClass(TextInputFormat.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		
		job.setOutputKeyClass(Key.class);
		job.setOutputValueClass(IntWritable.class);
		
		//MultipleOutputs.addNamedOutput(job, "sum", TextOutputFormat.class, codeKey.class, IntWritable.class);
		 
		
		
		job.waitForCompletion(true);
		return 0;
		
		
	}
	public static void main(String [] args) throws Exception{
	int res = ToolRunner.run(new Configuration(),new MinFeeDriver(),args);
	System.out.println("Result=========================================================" + res);
	
	
	}

}
