package minimumFee2;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;


public class MinFeeReducer extends Reducer<Key, IntWritable, Key, IntWritable>{
	

	private IntWritable result = new IntWritable();
	
	
	public void reduce(Key key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException{
	
	
		int minValue = Integer.MAX_VALUE;
		for(IntWritable value : values){
			minValue = Math.min(minValue, value.get());
		}
		result.set(minValue);
		context.write(key, result);
	}
}
