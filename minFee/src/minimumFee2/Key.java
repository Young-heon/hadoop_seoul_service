package minimumFee2;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;

public class Key implements WritableComparable <Key> {
	private String location;
	private Integer productNum;

	
	public Key(){}
	
	public Key(String location, Integer productNum){
		this.location = location;
		this.productNum = productNum;
	}
	
	public String getLocation(){
		return location;
	}
	public Integer getProductNum(){
		return productNum;
	}
	
	public void setLocation(String location){
		this.location = location;
	}
	public void setProductNum(Integer productNum){
		this.productNum = productNum;
	}
	
	
	@Override
	public String toString(){
		return (new StringBuilder()).append(",").append(location).append(",").append(productNum).append(",").toString();
	}
	
	@Override
	public void readFields(DataInput in) throws IOException{
		location = WritableUtils.readString(in);
		//location = in.readUTF();
		productNum = in.readInt();
	}
	
	@Override
	public void write(DataOutput out) throws IOException{
		WritableUtils.writeString(out,location);
		//out.writeUTF(location);
		out.writeInt(productNum);
	}
	
	@Override
	public int compareTo(Key key){
		int result = location.compareTo(key.location);
		if(0 == result)
		{
			result = productNum.compareTo(key.productNum);
		}
		return result;
	}
}
